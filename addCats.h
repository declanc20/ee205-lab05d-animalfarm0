////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - animal farm - EE 205 - Spr 2022
///
/// @file addCat.h
/// @version 1.0
///
/// @author Declan Campbell <declanc@hawaii.edu>
/// @date 18_Feb_2022
////////////////////////////////////////////////////////////////////////////

#pragma once

extern int addCat(char name[], enum Gender gender, enum Breed breed, bool isfixed, float weight);
