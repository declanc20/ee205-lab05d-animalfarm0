///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalfarm 0 - EE 205 - Spr 2022
///
/// Usage: This file has the function definition to delete all cats from the database
///
///
/// @file deleteCats.c
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/22/22
///////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include<string.h>
#include"catDataBase.h"
#include"deleteCats.h"

void deleteAllCats(){

    /*erase all database entries*/  
    char empty[2] = "";
    for (int i = numOfCats; i > 0; i--){ 
    strcpy(dataBase[i].name, empty);
    dataBase[i].gender = 0;
    dataBase[i].breed = 0;
    dataBase[i].isFixed = 0;
    dataBase[i].weight = 0;

    }

    numOfCats = 0; //set the high water mark to zero

}
