///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalfarm 0 - EE 205 - Spr 2022
///
/// Usage: used to create the cat dataBase 
///
///
/// @file main.c
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/22/22
///////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include<string.h>
#include<stdbool.h>
#include"catDataBase.h"
#include"addCats.h"
#include"reportCats.h"
#include"updateCats.h"
#include"deleteCats.h"




int main(){

   addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
   
   addCat( "Milo", MALE, MANX, true, 7.0 ) ;
   
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
   
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
   
   addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
   
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
   
   printAllCats();
   
   int kali = findCat( "Kali" ) ;
   printf("the value of kali is: %d\n", kali);//debug
   
   /*updateCatName( kali, "Chili" ) ; // this should fail
   printCat( kali );*/
   
   updateCatName( kali , "Capulet" );
   updateCatWeight( kali, 9.9 );
   fixCat( kali );
   printCat( kali );
   
   printAllCats();
   
   deleteAllCats();
   printAllCats();

   return 0;

}
