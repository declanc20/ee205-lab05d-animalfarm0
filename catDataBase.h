#include<stdio.h>
#include<stdbool.h>

#define MAX_CATS (100)
#define MAX_NAME (30)

 enum Gender{ UNKNOWN_GENDER, MALE, FEMALE };
 enum Breed{ UNKOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX}; 

struct Cat {  //database is 
   char name[MAX_NAME];
   enum Gender gender;
   enum Breed breed;
   bool isFixed;
   float weight; //cant be negative
   };  

extern struct Cat dataBase[MAX_CATS];

extern int numOfCats; //global variable for number of cats in database
