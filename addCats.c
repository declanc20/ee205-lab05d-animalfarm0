///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalfarm 0 - EE 205 - Spr 2022
///
/// Usage: @function definition to add cats to the database 
///
///
/// @file addCats.c
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/22/22
///////////////////////////////////////////////////////////////////////////////

#include<string.h>
#include<stdio.h>
#include<stdbool.h>
#include"catDataBase.h"
#include"addCats.h"

int addCat(char name[], enum Gender gender, enum Breed breed, bool isfixed, float weight ){
    bool flag = 0; //flag to check that all conditions passed remains 0 if all passed
  if (numOfCats >= MAX_CATS){ //if num of cats is greater than or equal to Max cats, flag
        flag = 1;
        }

  else if (strlen(name) == 0){  //if catname is empty, flag
     flag = 1;
  }


  else if (strlen(name) >= MAX_NAME){ //if cat name is longer than max name, flag
      flag = 1;
   }

  for (int i = 0; i < numOfCats; i++){   //check that cat name is unique
     if ((strcmp(dataBase[i].name, name)) == 0){
        flag = 1;
        break;
     }
  }


   if ( weight <=0 ) { //weight needs to be positive nonzero value
      flag = 1;
   }


   /*if all validation checks passed, fill database*/
   if (flag == 0){
    strcpy(dataBase[numOfCats].name,name);
    dataBase[numOfCats].gender = gender;
    dataBase[numOfCats].breed = breed;
    dataBase[numOfCats].isFixed = isfixed;
    dataBase[numOfCats].weight = weight;
    numOfCats++;
    return numOfCats;
   }

   /*if any validation check fails print error*/
   else{
      printf("Error cannot add cat: %d to database\n", numOfCats);
   }

   return 0;
}

