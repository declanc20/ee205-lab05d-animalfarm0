///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalfarm 0 - EE 205 - Spr 2022
///
///
/// @file updateCats.h
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/22/22
///////////////////////////////////////////////////////////////////////////////


extern void fixCat(int catNum);
extern void updateCatName(int catNum, char newName[]);
extern void updateCatWeight(int catNum, float newWeight);

