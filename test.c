#include<stdio.h>
#include<string.h>
#include<stdbool.h>

#define MAX_CATS (100)
#define MAX_NAME (30)


enum Gender{ UNKNOWN_GENDER, MALE, FEMALE };
enum Breed{ UNKOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX}; 

struct Cat {  //database is 
   char name[MAX_NAME];
   enum Gender gender;
   enum Breed breed;
   bool isFixed;
   float weight; //cant be negative
   };  

struct Cat dataBase[MAX_CATS];

int numOfCats=0; //global variable for number of cats in database

int addCat(char name[], enum Gender gender, enum Breed breed, bool isfixed, float weight );
int findCat( char name[]);
void printAllCats(void);
void printCat( int catNum);
void updateCatWeight(int catNum, float newWeight);
void deleteAllCats();
void fixCat(int catNum);
void updateCatName(int catNum, char newName[]);



   int main(){
 addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
addCat( "Milo", MALE, MANX, true, 7.0 ) ;
addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
printAllCats();
printf("\n \n");
int kali = findCat( "Kali" ) ;
printf("the value of kali is: %d\n", kali);//debug
/*updateCatName( kali, "Chili" ) ; // this should fail
printCat( kali );*/
updateCatName( kali , "Capulet" ) ;
updateCatWeight( kali, 9.9 ) ;
fixCat( kali ) ;
printCat( kali );
printAllCats();

printf("\n \n");
deleteAllCats();
printAllCats();
   return 0;
   }   


   
int addCat(char name[], enum Gender gender, enum Breed breed, bool isfixed, float weight ){
    bool flag = 0; //flag to check that all conditions passed remains 0 if all passed 
  if (numOfCats >= MAX_CATS){ //if num of cats is greater than or equal to Max cats, flag 
        flag = 1;
        }

  else if (strlen(name) == 0){  //if catname is empty, flag
     flag = 1;
  }


  else if (strlen(name) >= MAX_NAME){ //if cat name is longer than max name, flag
      flag = 1;
   }

  for (int i = 0; i < numOfCats; i++){   //check that cat name is unique
     if ((strcmp(dataBase[i].name, name)) == 0){
        flag = 1;
        break;
     }
  }


   if ( weight <=0 ) { //weight needs to be positive nonzero value
      flag = 1;
   }


   if (flag == 0){
    strcpy(dataBase[numOfCats].name,name);
    dataBase[numOfCats].gender = gender;
    dataBase[numOfCats].breed = breed;
    dataBase[numOfCats].isFixed = isfixed;
    dataBase[numOfCats].weight = weight;
    numOfCats++;
    return numOfCats;
   }

   else{
      printf("Error cannot add cat: %d to database\n", numOfCats);
   }

}

void printCat( int catNum){

   if (catNum < 0 || catNum > MAX_CATS-1){ //max_cats-1 because we start at cat 0 and go to 29 for indexes.
      printf("animalFarm0: Bad cat [%d]\n", catNum);
   }   

   else {
      printf("catindex=[%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] \n", catNum, dataBase[catNum].name, dataBase[catNum].gender, dataBase[catNum].breed, dataBase[catNum].isFixed, dataBase[catNum].weight);
   }   

}

void printAllCats(void){

   for (int i = 0; i < numOfCats; i++){  //numOfCats holds the array indexes so no need to minus
    
      printf("catindex=[%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] \n", i, dataBase[i].name, dataBase[i].gender, dataBase[i].breed, dataBase[i].isFixed, dataBase[i].weight);

   }   
}


int findCat( char name[]){
   
   int flag = 1; //flag unless match found

   for (int i = 0; i < numOfCats; i++){ //iterate through database looking for match
     if  (strcmp(dataBase[i].name, name) == 0){
      flag = 0;
      return i;
      break;
      }   
   }   

   if (flag == 1){ //if after going through the databse no match print error
      printf("no cat with that name in the database :/ \n");
   }   

}

void updateCatName(int catNum, char newName[]){

   int flag = 0;
   if(catNum > numOfCats){
      flag = 1;
      printf("that cat doesn't exist\n");
   }

   if (strlen(newName) == 0){
     flag = 1;
      printf("you didnt enter a name\n");
   }

   if (strlen(newName) >= MAX_NAME){
      printf("name is too long\n");
   }

   for (int i = 0; i < numOfCats; i++){   //check that cat name is unique
     if ((strcmp(dataBase[i].name, newName)) == 0){
        flag = 1;
        break;
     }
  }


   if (flag == 0){
   strcpy(dataBase[catNum].name,newName);
   }

}


void fixCat(int catNum){

   dataBase[catNum].isFixed = 1;
   
}


void updateCatWeight(int catNum, float newWeight){
    
    if (newWeight <= 0 ){
    printf("invalid weight\n");    
    }
    
    dataBase[catNum].weight = newWeight;

}


void deleteAllCats(){
    
    
    for (int i = numOfCats; i > 0; i--){
    //strcpy(dataBase[numOfCats].name, 0);
    dataBase[numOfCats].gender = 0;
    dataBase[numOfCats].breed = 0;
    dataBase[numOfCats].isFixed = 0;
    dataBase[numOfCats].weight = 0;    
        
    }
    
}
