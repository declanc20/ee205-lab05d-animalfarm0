///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalfarm 0 - EE 205 - Spr 2022
///
/// Usage: updates cats that have been "fixed" or neutered in the database and or
/// updates cat weight
/// and or cat name
///
/// @file updateCats.c
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/22/22
///////////////////////////////////////////////////////////////////////////////


#include"catDataBase.h"
#include<stdio.h>
#include<string.h>
#include"updateCats.h"

void updateCatName(int catNum, char newName[]){

   int flag = 0;
   if(catNum > numOfCats){
      flag = 1;
      printf("that cat doesn't exist\n");
   }

   if (strlen(newName) == 0){
     flag = 1;
      printf("you didnt enter a name\n");
   }

   if (strlen(newName) >= MAX_NAME){
      printf("name is too long\n");
      flag =1;
   }

   for (int i = 0; i < numOfCats; i++){   //check that cat name is unique
     if ((strcmp(dataBase[i].name, newName)) == 0){
        flag = 1;
        break;
     }
  }


   if (flag == 0){
   strcpy(dataBase[catNum].name,newName);
   }

}


void fixCat(int catNum){

   dataBase[catNum].isFixed = 1;

}


void updateCatWeight(int catNum, float newWeight){

    if (newWeight <= 0 ){
    printf("invalid weight\n");
    }
   
    else{
    dataBase[catNum].weight = newWeight;
    }

}
