///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05d - animalfarm 0 - EE 205 - Spr 2022
///
/// Usage: three functions...
/// one function to print all cats in the database
/// one to find a specific cat wihtin the database
/// one to print an individual cat in the database
///
///
/// @file reportCats.c
/// @version 1.0
///
///
/// @author Declan Campbell < declanc@hawaii.edu>
/// @date   2/22/22
///////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include"catDataBase.h"
#include<string.h>
#include"reportCats.h"
void printCat( int catNum){

   /*print bad cat for trying to find a cat outside of the range of database*/
   if (catNum < 0 || catNum > MAX_CATS-1){ //max_cats-1 because we start at cat 0 and go to 29 for indexes.
      printf("animalFarm0: Bad cat [%d]\n", catNum);
   }

   else {
      printf("catindex=[%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] \n", catNum, dataBase[catNum].name, dataBase[catNum].gender, dataBase[catNum].breed, dataBase[catNum].isFixed, dataBase[catNum].weight);
   }

}

void printAllCats(void){

   for (int i = 0; i < numOfCats; i++){

      printf("catindex=[%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] \n", i, dataBase[i].name, dataBase[i].gender, dataBase[i].breed, dataBase[i].isFixed, dataBase[i].weight);

   }
}


int findCat( char name[]){

   int flag = 1; //flag unless match found

   for (int i = 0; i < numOfCats; i++){ //iterate through database looking for match
     if  (strcmp(dataBase[i].name, name) == 0){
      flag = 0;
      return i;
      break;
      }
   }

   if (flag == 1){ //if after going through the databse no match print error
      printf("no cat with that name in the database :/ \n");
   return 37;
   }
   
   return 0;
}
